import { Injectable } from '@angular/core';
import { HEROES } from './mock-heroes';
import { Hero, FMyItemInfo, FCategoryInfoLevelOne } from './hero';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HeroService {
  private headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  private heroesUrl = 'http://real.fuwo.com/model/label/tree/';
  private SIGNIN_URL = 'http://real.fuwo.com/account/login/username=jinxing.zhang@fuwo.com&password=123456&platform_no=-1' ;
  private  tagTreeUrl =  'http://real.fuwo.com/model/tag/tree/?label=';
  private  hello;
  constructor(private http: Http) { }

  LoginService(): Promise<string> {
    return this.http
      .post("http://real.fuwo.com/account/login/","username=jinxing.zhang@fuwo.com&password=123456&platform_no=-1",{headers: this.headers})
      .toPromise()
      .then(response => 'success');
  }

  getMyItems(): Promise<any[]> {
    return  this.http.get(this.heroesUrl)
      .toPromise()
      .then(response =>  response.json()['data']['record']['children'] as any[])
      // var ItemInfoArray = new Array<FMyItemInfo>();
      // var ItemInfo = new FMyItemInfo();
      //
      // for(var i = 0; i < result.length; i++) {
      //   ItemInfo.name = result[i]['name'];
      //   ItemInfo.label = result[i]['label'];
      //   ItemInfoArray.push(ItemInfo);
      // }
      .catch(this.handleError);
  }

  getTagTree(id: number): Promise<any[]> {
    let url = this.tagTreeUrl + id;
    return  this.http.get(url,{withCredentials: true} )
      .toPromise()
      .then(response =>  response.json()['data']['record']['children'] as any[])
      .catch(this.handleError);
  }

  getHeroes(): Promise<Hero[]> {
    return Promise.resolve(HEROES);
  } // stub

  getCategoryID(label:string,Tag:string,leaves:string):Promise<any[]> {
    let url = 'http://real.fuwo.com/model/item/list/?label='+ label;
    url = url + '&tag_id=' + Tag;
    url = url + '&need_leaves=' + leaves;
    console.info(url);
    return  this.http.get(url,{withCredentials: true} )
      .toPromise()
      .then(response =>  response.json()['data']['records'] as any[])
      .catch(this.handleError);
  }

  getTagName(TagIds: any[]):Promise<any[]> {
    let url = 'http://real.fuwo.com/model/tag/detail/?tag_ids=';

    for(var i = 0; i < TagIds.length; i++) {
      if(i === 0) {
        url = url + TagIds[i];
      }
      else{
        url = url + '&tag_ids='+ TagIds[i];
      }
    }
    console.info(url);
    return  this.http.get(url,{ withCredentials: true } )
      .toPromise()
      .then(response =>  response.json() as any[])
      .catch(this.handleError);
  }

  getCategoryName(CategoryIds: any[]):Promise<any[]> {
    let url = 'http://real.fuwo.com/model/category/detail/?category_ids=';

    for(var i = 0; i < CategoryIds.length; i++) {
      if(i === 0) {
        url = url + CategoryIds[i];
      }
      else{
        url = url + '&category_ids='+ CategoryIds[i];
      }
    }
    console.info(url);
    return  this.http.get(url,{ withCredentials: true } )
      .toPromise()
      .then(response =>  response.json() as any[])
      .catch(this.handleError);
  }

  GetImageUrlFromNoesArray(NoesArray:any[]):Promise<any[]>  {

    let url = 'http://real.fuwo.com/model/item/detail/?noes=';
    for(var i = 0; i < NoesArray.length;i++) {
      if (i === 0) {
        url = url + NoesArray[i];
      }
      else {
        url = url + '&noes='+NoesArray[i];
      }
    }
    return  this.http.get(url,{withCredentials: true} )
      .toPromise()
      .then(response =>  response.json()['data']['records'] as any[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


