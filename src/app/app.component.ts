import { Component,OnInit} from '@angular/core';
import { Hero, FMyItemInfo, FItemSmallLabel, FCategoryInfoLevelOne,FCategoryInfoLevelTwo } from './hero';
import { HeroService } from './hero.service';
import {PanelMenuModule,MenuItem,TieredMenuModule,DataGridModule,DialogModule,MenubarModule} from 'primeng/primeng';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `
    <h1>{{title}}</h1>
    <button (click)="Login()"value = 'Login' style="height:50px;width:100px;">Login</button>
    <h3>{{loginStatus}}</h3>
    <button (click)="GetMyItems()"value = 'GetItem' style="height:50px;width:100px;">ModelLibrary</button>
    <div id = "left1">
      <p-panelMenu [model]="items" [style]="{'width':'120px'}"></p-panelMenu>
    </div>
   <div id = "left2" *ngIf="CategoryOne">
     <p-tieredMenu [model]="CategoryOne" [style]="{'width':'120px'}"></p-tieredMenu>
   </div>
    
    <div id = "left3" *ngIf="ImageUrlArray">
      <p-menubar [model]="Categorys">
      </p-menubar>
    <p-dataGrid [value]="ImageUrlArray" [paginator]="false" [rows]="20" >
      <p-header>
       
      </p-header>
      <ng-template let-car pTemplate="item">
        <div style="padding:3px" class="ui-g-12 ui-md-6">
          <p-panel [style]="{'text-align':'center'}">
            <img src="{{car.preview_url}}" width="60">
            <hr class="ui-widget-content" style="border-top:0">
            <i class="fa fa-search" (click)="selectCar(car)" style="cursor:pointer"></i>
          </p-panel>
        </div>
      </ng-template>
    </p-dataGrid>
      
      <p-paginator rows="20" [totalRecords]="NoesArray.length" (onPageChange)="OnPageChangeEvent($event)"></p-paginator>
    </div>

    <p-dialog header="Car Details" [(visible)]="displayDialog" [responsive]="true" showEffect="fade" [modal]="true" width="400" (onAfterHide)="onDialogHide()">
      <div class="ui-grid ui-grid-responsive ui-grid-pad" *ngIf="selectedCar" style="font-size:16px;text-align:center;padding:20px">
        <div class="ui-grid-row">
          <div class="ui-grid-col-12" style="text-align:center"><img src="{{selectedCar.preview_url}}" height="200" width="200"></div>
        </div>
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">名称: </div>
          <div class="ui-grid-col-8">{{selectedCar.name}}</div>
        </div>
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">品牌: </div>
          <div class="ui-grid-col-8">{{selectedCar.year}}</div>
        </div>
        
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">规格: </div>
        </div>
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">宽: </div>
          <div class="ui-grid-col-8">{{selectedCar.width}} cm</div>
        </div>
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">长: </div>
          <div class="ui-grid-col-8">{{selectedCar.length}} cm</div>
        </div>
        <div class="ui-grid-row">
          <div class="ui-grid-col-4">高: </div>
          <div class="ui-grid-col-8">{{selectedCar.height}} cm</div>
        </div>
      </div>
    </p-dialog>
  `,
  providers: [HeroService]
})
export class AppComponent implements OnInit {

  title = 'Model Library';
  loginStatus = 'not login';
  heroes: Hero[];
  ItemInfoArray: any[];
  TagTree: any[];
  selectedHero: Hero;
  CategoryItems: any[];
  NoesArray: any[];
  ImageUrlArray: any[];
  items: MenuItem[];
  CategoryOne: MenuItem[];
  Categorys: MenuItem[];
  selectedCar: any;

  displayDialog: boolean;
  selectCar(car: any) {
    this.selectedCar = car;
    this.displayDialog = true;
  }

  onDialogHide() {
    this.selectedCar = null;
  }

  OnPageChangeEvent(event: any) {
    console.info(event);
    let temp = this.NoesArray.slice(event.first,event.first + 20);
    this.heroService.GetImageUrlFromNoesArray(temp).then(ImageUrl => {
      this.ImageUrlArray = ImageUrl;
      console.info(this.ImageUrlArray);
    });
  }

  public GetTagTree(label: any) {
    this.heroService.getTagTree(label).then(TagTreeArray => {
      this.TagTree = TagTreeArray;
      this.CategoryOne = [];
      for(var i = 0; i < TagTreeArray.length; i++) {
        let CategoryOneTemp: MenuItem = {};
        CategoryOneTemp.label = TagTreeArray[i]['name'];
        CategoryOneTemp.target = TagTreeArray[i]['tag_id'];
        CategoryOneTemp.title = '1';
        CategoryOneTemp.style = label;
        CategoryOneTemp.command =(event:any) =>{
          console.info(event);
          this.CategoryButtonClicked(event['item']['style'],event['item']['target'],event['item']['title']);
        };
        this.CategoryOne.push(CategoryOneTemp);
        let CategoryOneTempItems: MenuItem[] = [];

        if(TagTreeArray[i]['children'].length > 0)
          CategoryOneTemp.items = CategoryOneTempItems;

        for(var j = 0; j < TagTreeArray[i]['children'].length; j++) {
          let CategoryTwoTemp: MenuItem = {};
          CategoryTwoTemp.label = TagTreeArray[i]['children'][j]['name'];
          CategoryTwoTemp.target = TagTreeArray[i]['children'][j]['tag_id'];
          CategoryTwoTemp.title = '0';
          CategoryTwoTemp.style = label;
          CategoryTwoTemp .command =(event:any) =>{
            console.info(event);
            this.CategoryButtonClicked(event['item']['style'],event['item']['target'],event['item']['title']);
          };
          CategoryOneTempItems.push(CategoryTwoTemp);
          let CategoryTwoTempItems: MenuItem[] = [];

          if(TagTreeArray[i]['children'][j]['children'].length > 0)
            CategoryTwoTemp.items = CategoryTwoTempItems;

          for(var k = 0; k < TagTreeArray[i]['children'][j]['children'].length;k++) {
            let CategoryThreeTemp: MenuItem = {};
            CategoryThreeTemp.label = TagTreeArray[i]['children'][j]['children'][k]['name'];
            CategoryThreeTemp.target = TagTreeArray[i]['children'][j]['children'][k]['tag_id'];
            CategoryThreeTemp.title = '0';
            CategoryThreeTemp.style = label;
            CategoryThreeTemp .command =(event:any) => {
              console.info(event);
              this.CategoryButtonClicked(event['item']['style'],event['item']['target'],event['item']['title']);
            };
            CategoryTwoTempItems.push(CategoryThreeTemp);
          }
        }
      }
      console.info(this.TagTree);
    });
  }

  public FilterNoes(event: any) {
    console.info("filter");
    console.info(event);
    console.info(this.CategoryItems);

    this.NoesArray = [];

    for(var key in this.CategoryItems['noes']) {
      for(var i in this.CategoryItems['noes'][key]){
        for(var j = 0; j < this.CategoryItems['noes'][key][i].length;j++) {
          //console.info(this.CategoryItems['noes'][key][i][j].toString())
          if(this.CategoryItems['noes'][key][i][j].toString() === event['item']['target'].toString()) {
            this.NoesArray.push(i);
          }
        }
      }
    }

    let temp = this.NoesArray.slice(0,20);
    this.heroService.GetImageUrlFromNoesArray(temp).then(ImageUrl => {
      this.ImageUrlArray = ImageUrl;

    });


  }

  public CategoryButtonClicked(label: any, tagId: any, leaves: any) {

    this.heroService.getCategoryID(label, tagId, leaves).then(CategoryItemTemp => {
      this.CategoryItems = CategoryItemTemp;
      this.NoesArray = [];
      this.Categorys = [];

      for(var key in CategoryItemTemp['noes']) {
        for(var i in CategoryItemTemp['noes'][key]){
          this.NoesArray.push(i);
        }
      }

      let categoryIdsTemp: any[] = [];
      let TagIdsTemp: any[] = [];

      for (var key in CategoryItemTemp['categories']) {
        for (var i in CategoryItemTemp['categories'][key]) {
          let temp: MenuItem = { };

          temp.target = i;
          categoryIdsTemp.push(i);

          let tempItems: MenuItem[] = [];
          temp.items = tempItems;
          this.Categorys.push(temp);

          for(var j in  CategoryItemTemp['categories'][key][i]) {
            let tempItemsItem:MenuItem = {};
            tempItemsItem.target =  CategoryItemTemp['categories'][key][i][j];
            TagIdsTemp.push(tempItemsItem.target);
            tempItems.push(tempItemsItem);
          }
        }
      }

      this.heroService.getCategoryName(categoryIdsTemp).then(CategoryNameTemp=> {
        console.info(CategoryNameTemp);
        for(var index = 0; index < this.Categorys.length; index++) {
          for(var index2 = 0; index2 < CategoryNameTemp['data']['records'].length; index2++) {
            if(CategoryNameTemp['data']['records'][index2]['category_id'].toString() === this.Categorys[index].target) {
              this.Categorys[index].label = CategoryNameTemp['data']['records'][index2]['name'];
              break;
            }

          }
        }
      });

      this.heroService.getTagName(TagIdsTemp).then(TagNameTemp=> {
       console.info(TagNameTemp);
        for(var index = 0; index < this.Categorys.length; index++) {
          let temp: MenuItem[] = this.Categorys[index].items as MenuItem[];
          for (var index3 = 0; index3 < temp.length; index3++) {
            for(var index2 = 0; index2 < TagNameTemp['data']['records'].length; index2++) {
              if(TagNameTemp['data']['records'][index2]['tag_id'].toString() === temp[index3].target.toString()) {
                temp[index3].label = TagNameTemp['data']['records'][index2]['name'];
                temp[index3].command = (event: any) => {
                  this.FilterNoes(event);
                };
                break;
              }
            }
          }
          //this.Categorys[index].items = temp;
        }
      });

      console.info("Noes");
      console.info(this.CategoryItems);
      let temp = this.NoesArray.slice(0,20);
      this.heroService.GetImageUrlFromNoesArray(temp).then(ImageUrl => {
        this.ImageUrlArray = ImageUrl;

      });

    });
  }


  constructor(private heroService: HeroService) {

  }
  Login(): void {
    this.heroService.LoginService().then((str => this.loginStatus = str));
  }

  GetMyItems(): void {
    this.heroService.getMyItems().then(ItemArray => {
      this.ItemInfoArray = ItemArray;
      this.items = [];
      let MyItem: MenuItem = {};
      let OtherItem: MenuItem = {};
      OtherItem.label = '其他';
      let OtherItemItems: MenuItem[] = [];
      OtherItem.items = OtherItemItems;
      this.items.push(MyItem);
      this.items.push(OtherItem);

      for(var i = 0; i <  this.ItemInfoArray.length; i++) {
        let ItemItems: MenuItem[] = [];
        if(this.ItemInfoArray[i]['children'].length > 0) {
          MyItem.items = ItemItems;
          MyItem.label = this.ItemInfoArray[i]['name'];
          for(var j = 0; j < this.ItemInfoArray[i]['children'].length; j++) {
            let temp:MenuItem = {};
            temp.label = this.ItemInfoArray[i]['children'][j]['name'];
            temp.target = this.ItemInfoArray[i]['children'][j]['label'];
            temp.command = (event:any) =>{
              let label = event['item']['target'];
              this.GetTagTree(label);
            };
            ItemItems.push(temp);
          }
          MyItem.items = ItemItems;
        }
        else{
          let temp:MenuItem = {};
          temp.label = this.ItemInfoArray[i]['name'];
          temp.target = this.ItemInfoArray[i]['label'];
          temp.command = (event:any) =>{
            let label = event['item']['target'];
            console.info(event);
            this.GetTagTree(label);
          };
          OtherItemItems.push(temp);
        }

      }
      console.log('++++++');
      console.info(this.ItemInfoArray);
      console.info(this.items);
    });
  }

  ngOnInit(): void {
  //  this.ImageUrlArray = [];
    this.NoesArray = [];
    // this.items = [];
    // this.CategoryOne = [];
  }
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
}
