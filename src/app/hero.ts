export class FItemSmallLabel {
  ID: number;
  name: string;
  label: string;
}

export class FMyItemInfo {
  label: number;
  name: string;
  ChildItemArray: FItemSmallLabel[];
}

export class FCategoryInfoLevelThree {
  Name: string;
  TagsId: number;
}


export class FCategoryInfoLevelTwo {
  Name: string;
  TagsId: number;

  CategoryLevelThree: FCategoryInfoLevelThree[];
}

export  class FCategoryInfoLevelOne {
  Name: string;
  TagsId: number;
  CategoryLevelTwo: FCategoryInfoLevelTwo[];

}


export class Hero {
  id: number;
  name: string;
}
