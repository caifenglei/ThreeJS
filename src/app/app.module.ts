import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeroDetailComponent } from './hero-detail.component';
// Import the Animations module
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ButtonModule} from 'primeng/primeng';
import {AccordionModule} from 'primeng/primeng';
import {PanelMenuModule} from 'primeng/primeng';
import {MenuModule} from 'primeng/primeng';
import {TieredMenuModule} from 'primeng/primeng';
import {DataGridModule} from 'primeng/primeng';
import {PanelModule} from 'primeng/primeng';
import {DialogModule} from 'primeng/primeng';
import {PaginatorModule} from 'primeng/primeng';
import {MenubarModule} from 'primeng/primeng';
@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    // Register the modules
    BrowserAnimationsModule,
    ButtonModule,
    AccordionModule,
    PanelMenuModule,
    MenuModule,
    TieredMenuModule,
    DataGridModule,
    PanelModule,
    DialogModule,
    PaginatorModule,
    MenubarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
